# Cloud-Press Pro Site Enabler #

### What is this for? ###

[Cloud-Press](http://cloud-press.net) offers a "pro" website feature that allows you to export your website to a remote server.
This plugin enables the pro features on an individual site instead of your overall account for absolutely no cost by exploiting a vulnerability in their system.

### How do I get set up? ###

* Create a [Cloud-Press](http://cloud-press.net) account by going [here](http://cloud-press.net)
* Create a [Cloud-Press](http://cloud-press.net) website
* [Download the plugin](https://bitbucket.org/notmike101/cloud-press-pro-enabler/downloads/) by going [here](https://bitbucket.org/notmike101/cloud-press-pro-enabler/downloads/)
* Install the plugin
* Enable the plugin

To revert back to your normal free website plan, just disable the plugin.

### CAUTION ###

This plugin violates Cloud-Press's terms of service and should not be used in an actual product environment.  This plugin is used for demonstration purposes of the vulnerability itself, not for general use.  Usage is highly unrecommended, and I will hold no responsibility for any malicious uses of this plugin by a third party.